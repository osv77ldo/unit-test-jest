const express = require('express');
const axios = require('axios');
const parser = require('body-parser');
const bodyParser = require('body-parser');
const { users } = require('./routes');
const app = express();
const port = 3000;

app.use(bodyParser.urlencoded({ extended:false }));

app.use(parser.json());

const userHandler = users({axios});

app.get('/', userHandler.get);

app.post('/', userHandler.post );

app.put('/:id', userHandler.put);

app.delete('/:id', userHandler.delete);

app.listen(port, () => console.log(`Example app listening at http://localhost:${port}`));