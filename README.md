# Express Server and Unitary test with Jest

This project is a simple REST API created with express and tested with Jest

## Project Setup

  Install dependencies:

```bash
$ npm install
```


## Run the tests

  To run the test suite, first install the dependencies, then run `npm test`:

```bash
$ npm test
```
